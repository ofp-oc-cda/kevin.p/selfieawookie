﻿using Microsoft.EntityFrameworkCore;
using SelfieAWookie.Core.Framework;
using SelfieAWookie.Core.Selfies.Domain;
using SelfieAWookie.Core.Selfies.Infrastructures.Data.TypeConfigurations;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SelfieAWookie.Core.Selfies.Infrastructures.Data
{
    public class SelfiesContext : DbContext, IUnitOfWork
    {
        #region Constructors
        public SelfiesContext([NotNullAttribute] DbContextOptions options) : base(options) { }
        public SelfiesContext() : base() { }
        #endregion

        #region Internal methods
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new SelfieEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new WookieEntityTypeConfiguration());
        }
        #endregion

        #region Properties
        public DbSet<Selfie> Selfie { get; set; }
        public DbSet<Wookie> Wookies { get; set; }
        public DbSet<Picture> Picture { get; set; }
        #endregion
    }
}
