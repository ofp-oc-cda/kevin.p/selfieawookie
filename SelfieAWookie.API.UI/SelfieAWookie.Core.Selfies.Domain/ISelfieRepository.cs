﻿using SelfieAWookie.Core.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SelfieAWookie.Core.Selfies.Domain
{
    /// <summary>
    /// Repository to manage selfies
    /// </summary>
    public interface ISelfieRepository: IRepository
    {
        /// <summary>
        /// Gets all selfies
        /// </summary>
        /// <returns></returns>
        ICollection<Selfie> Getall(int wookieId);
        /// <summary>
        /// Add one selfie in database
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        Selfie AddOne(Selfie item);
        /// <summary>
        /// Create new picture
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        Picture AddOnePicture(string url);
    }
}
