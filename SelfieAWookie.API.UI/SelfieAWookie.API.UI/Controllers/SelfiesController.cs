﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SelfieAWookie.API.UI.Applications.DTOs;
using SelfieAWookie.Core.Selfies.Domain;
using SelfieAWookie.Core.Selfies.Infrastructures.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace SelfieAWookie.API.UI.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class SelfiesController : ControllerBase
    {
        #region Fields
        private readonly ISelfieRepository _repository = null;
        private readonly IWebHostEnvironment _hostEnvironment = null;

        #endregion

        #region Constructor
        public SelfiesController(ISelfieRepository repository, IWebHostEnvironment hostEnvironment)
        {
            this._repository = repository;
            this._hostEnvironment = hostEnvironment;
        }
        #endregion

        #region Public methods
        [HttpGet]
        public IActionResult GetAll([FromQuery]int wookieId)
        {

            var selfiesList = this._repository.Getall(wookieId);
            var model = selfiesList.Select(item => new SelfieResumeDto() { Title = item.Title, WookieId = item.Wookie.Id, nbSelfiesFromWookie = (item.Wookie?.Selfies?.Count).GetValueOrDefault(0) }).ToList();
            return this.Ok(model);
        }

        [Route("Pictures")]
        [HttpPost]
        public async Task<IActionResult> AddPicture(IFormFile picture)
        {
            string filePath = Path.Combine(this._hostEnvironment.ContentRootPath, "images\\selfies");
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            filePath = Path.Combine(filePath, picture.FileName);
            using var stream = new FileStream(filePath, FileMode.OpenOrCreate);
            await picture.CopyToAsync(stream);
            var itemFile = this._repository.AddOnePicture(filePath);
            this._repository.UnitOfWork.SaveChanges();
            return this.Ok(itemFile);
        }

        [HttpPost]
        public IActionResult AddOne(Selfie selfie)
        {
            IActionResult result = this.BadRequest();
            Selfie addedSelfie = this._repository.AddOne(new Selfie()
            {
                ImagePath = selfie.ImagePath,
                Title = selfie.Title
            });
            this._repository.UnitOfWork.SaveChanges();
            if (addedSelfie != null)
            {
                selfie.Id = addedSelfie.Id;
                result = this.Ok(selfie);
            }
            return result;
        }
        #endregion

    }
}
