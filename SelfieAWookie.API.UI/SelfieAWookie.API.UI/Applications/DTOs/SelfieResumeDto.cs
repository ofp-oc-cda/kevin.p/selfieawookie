﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SelfieAWookie.API.UI.Applications.DTOs
{
    public class SelfieResumeDto
    {
        #region Properties
        public int nbSelfiesFromWookie { get; set; }
        public string Title { get; set; }
        public int WookieId { get; set; }
        #endregion
    }
}
